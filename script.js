console.log('Hello world');
let jedi = "Revan";
let aujourdhui = new Date();
let planetes = [
        "Tatooine",
        "Dagobah",
        "Hoth"
    ];
console.log(jedi);
console.dir(aujourdhui);
console.table(planetes);

//boucle for sur le tableau précédent
for (const planete of planetes) {
    console.dir(planete);
}

//boucle "si" pour voir si le jour est pair
if (aujourdhui.getDay() % 2) {
    console.log("Le jour est pair");
}

//fonction, créer une variable pour la retourner
function addition (nbre1, nbre2) {
    return nbre1 + nbre2;
}
console.log(addition(30, 12));