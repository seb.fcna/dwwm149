//création du mot à rechercher en remplaçant ses lettres par des "-"
const motAtrouver = "DWWM";
document.getElementById('motAtrouver').textContent = motAtrouver.replace(/\S/g, '_');
init();

//créer un alphabet de a à z avec la méthode "ascii"
//les lettres ont une valeur, allant de 65 (=a) à 90 (=z)
//il faut avoir dans le fichier html une balise div dans le body avec comme id="alphabet"
function init() {
    for (let i = 65; i <= 90; i++) {
        const lettre = String.fromCharCode(i);
        const bouton = document.createElement('button');
        bouton.textContent = lettre;
        //affiche dans la console la lettre cliqué
        bouton.addEventListener('click',
            () => clicSurLaLettre(lettre));
        document.getElementById('alphabet').appendChild(bouton);
    }
}

//fonction qui indique que l'on a bien cliqué sur une console de la page web
function clicSurLaLettre(lettre) {
    console.log("J'ai cliqué sur la lettre" + lettre);
    if(motAtrouver.includes(lettre)) {
        afficherLettre(lettre);
    } else {
        let nbEssai = document.getElementById('essaisRestant').innerText;
        nbEssai--;
        document.getElementById('essaisRestant').innerText = nbEssai;
    }
}


//essayer d'afficher la lettre juste
function afficherLettre(lettre) {
    //je prends le mot dans le html
    let motDom = document.getElementById('motAtrouver').textContent.split('');
   //je boucle sur chaque lettre du mot
    for(let i=0; i<motAtrouver.length; i++) {
        //si c'est la bonne lettre je remplace le '_' par la lettre
        if(motAtrouver[i] === lettre) {
            motDom[i] = lettre;
        }
    }
    //et je mets à jour le DOM
    document.getElementById('motAtrouver').textContent = motDom.join('');
}
