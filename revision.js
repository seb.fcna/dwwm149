//créer une liaison avec le bouton 
let monSuperBouton = document.getElementById('bSubmit');
monSuperBouton.addEventListener('click', ajouter);

function ajouter() {
//récupérer la valeur de l'input
    let monInput = document.getElementById('iTodo');
    let valeurDeLinput = monInput.value;

//créer un nouvel élément
    let nouvelElement = document.createElement('li');
    nouvelElement.innerText = valeurDeLinput;

//attacher le nouvel élément au DOM
    let monUl = document.getElementById('todos');
    monUl.appendChild(nouvelElement);
}
